@echo off
if not "%1" == "max" start /MAX cmd /c %0 max & exit/b
color 1E
chcp 65001 > nul

title Onyx Réparation erreur 27502

echo *********************************************************
echo *                                                       *
echo *              Résolution de l'erreur 27502             *
echo *                     Obsidian Onyx                     *
echo *                                                       *
echo *                   Par Dorian GARDES                   *
echo *                      07 Mars 2020                     *
echo *                         V1.0                          *
echo * https://gitlab.com/d-gardes/obsidian-onyx-error-27502 *
echo *                                                       *
echo *********************************************************
echo.
echo Vérification des droits d'administration
echo.

net session >nul 2>&1
if %errorLevel% == 0 (
    echo Suppression des bases de données
    echo.
    sqllocaldb unshare .\ObsidianOnyx
    sqllocaldb delete ObsidianControlSystems
    sqllocaldb delete ObsidianOnyx
    echo.
    echo Création de nouvelles bases de données
    echo.
    sqllocaldb create "ObsidianControlSystems"
    sqllocaldb share ObsidianControlSystems ObsidianOnyx
    echo.
    echo Bases de données recrées.
    echo Vous pouvez réessayer de désinstaller Onyx
    echo.

) else (
    echo Les droits d'administration sont nécéssaires.
    echo Lancez ce script en administrateur !
    echo.
)

pause