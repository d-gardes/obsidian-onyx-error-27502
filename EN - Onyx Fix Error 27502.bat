@echo off
if not "%1" == "max" start /MAX cmd /c %0 max & exit/b
color 1E
chcp 65001 > nul

title Onyx repare error 27502

echo *********************************************************
echo *                                                       *
echo *                 Solver for error 27502                *
echo *                     Obsidian Onyx                     *
echo *                                                       *
echo *                    By Dorian GARDES                   *
echo *                     March 07 2020                     *
echo *                         V1.0                          *
echo * https://gitlab.com/d-gardes/obsidian-onyx-error-27502 *
echo *                                                       *
echo *********************************************************
echo.
echo Checking elevated privileges
echo.

net session >nul 2>&1
if %errorLevel% == 0 (
    echo Deleting databases
    echo.
    sqllocaldb unshare .\ObsidianOnyx
    sqllocaldb delete ObsidianControlSystems
    sqllocaldb delete ObsidianOnyx
    echo.
    echo Creating new databases
    echo.
    sqllocaldb create "ObsidianControlSystems"
    sqllocaldb share ObsidianControlSystems ObsidianOnyx
    echo.
    echo Databases successfully created.
    echo Now you can try to uninstall Onyx
    echo.

) else (
    echo Elevated privileges needed.
    echo Run this script in admin !
    echo.
)

pause
